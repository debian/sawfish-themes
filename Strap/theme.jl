;; theme file, written Tue Apr 17 17:16:27 2001
;; created by sawfish-themer -- DO NOT EDIT!
;
; This file is part of the Strap Sawfish theme.
;
; Strap is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; Strap is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this theme; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(require 'make-theme)

(let
    ((patterns-alist
      '(("Knob"
         (inactive
          "unsel:knob-up.png")
         (focused
          "knob-up.png")
         (clicked
          "knob-dn.png"))
        ("VerticalBar"
         (inactive
          "unsel:vbar.png")
         (focused
          "vbar.png"))
        ("HorizontalBar"
         (inactive
          "unsel:hbar.png")
         (focused
          "hbar.png"))
        ("TopLeft"
         (inactive
          "unsel:tl.png"
          (border
           4
           4
           0
           0))
         (focused
          "tl.png"
          (border
           4
           4
           0
           0)))
        ("Title"
         (inactive
          "unsel:title.png"
          (border
           0
           4
           0
           0))
         (focused
          "title.png"
          (border
           0
           4
           0
           0)))
        ("Close"
         (inactive
          "unsel:close-up.png")
         (focused
          "close-up.png")
         (clicked
          "close-dn.png")
         (inactive-clicked
          "unsel:close-dn.png"))
        ("Minimize"
         (inactive
          "unsel:min-up.png")
         (focused
          "min-up.png")
         (clicked
          "min-dn.png")
         (inactive-clicked
          "unsel:min-dn.png"))
        ("Maximize"
         (inactive
          "unsel:max-up.png")
         (focused
          "max-up.png")
         (clicked
          "max-dn.png")
         (inactive-clicked
          "unsel:max-dn.png"))
        ("Shade"
         (inactive
          "unsel:shade-up.png")
         (focused
          "shade-up.png")
         (clicked
          "shade-dn.png")
         (inactive-clicked
          "unsel:shade-dn.png"))
        ("Shade2"
         (inactive
          "unsel:shade2-up.png")
         (focused
          "shade2-up.png")
         (highlighted
          "shade2-up.png")
         (clicked
          "shade2-dn.png")
         (inactive-clicked
          "unsel:shade2-dn.png"))
        ("br-shade"
         (inactive
          "unsel:br-shaded.png")
         (focused
          "br-shaded.png"))))

     (frames-alist
      '(("Default"
         ((top-edge . -10)
          (right-edge . -6)
          (class . top-right-corner))
         ((background . "VerticalBar")
          (top-edge . -2)
          (right-edge . -4)
          (bottom-edge . 2)
          (class . right-border))
         ((right-edge . -6)
          (bottom-edge . -4)
          (background . "Knob")
          (class . bottom-right-corner))
         ((bottom-edge . -4)
          (left-edge . -6)
          (background . "Knob")
          (class . bottom-left-corner))
         ((left-edge . -4)
          (bottom-edge . 2)
          (top-edge . 2)
          (background . "VerticalBar")
          (class . left-border))
         ((right-edge . 2)
          (left-edge . 2)
          (bottom-edge . -2)
          (background . "HorizontalBar")
          (class . bottom-border))
         ((width . 50)
          (left-edge . -8)
          (top-edge . -12)
          (background . "TopLeft"))
         ((right-edge . -6)
          (font . "-macromedia-bazaronite-medium-r-normal-*-*-90-*-*-p-*-ascii-0")
          (top-edge . -12)
          (left-edge . 42)
          (y-justify . 1)
          (x-justify . 5)
          (text . window-name)
          (background . "Title")
          (class . title))
         ((width . 13)
          (right-edge . 2)
          (top-edge . -8)
          (class . top-border))
         ((top-edge . -9)
          (left-edge . -4)
          (background . "Close")
          (class . close-button))
         ((top-edge . -9)
          (left-edge . 8)
          (background . "Minimize")
          (class . iconify-button))
         ((left-edge . 20)
          (top-edge . -9)
          (background . "Maximize")
          (class . maximize-button))
         ((class . shade-button)
          (top-edge . -9)
          (left-edge . 32)
          (background . "Shade")))
        ("Unframed")
        ("Shaped"
         ((top-edge . -10)
          (right-edge . -6)
          (class . top-right-corner))
         ((right-edge . -3)
          (left-edge . 40)
          (top-edge . -1)
          (background . "HorizontalBar")
          (class . bottom-border))
         ((width . 50)
          (left-edge . -8)
          (top-edge . -12)
          (background . "TopLeft"))
         ((right-edge . -6)
          (font . "-macromedia-bazaronite-medium-r-normal-*-*-90-*-*-p-*-ascii-0")
          (top-edge . -12)
          (left-edge . 42)
          (y-justify . 1)
          (x-justify . 5)
          (text . window-name)
          (background . "Title")
          (class . title))
         ((width . 13)
          (right-edge . 2)
          (top-edge . -8)
          (class . top-border))
         ((top-edge . -9)
          (left-edge . -4)
          (background . "Close")
          (class . close-button))
         ((top-edge . -9)
          (left-edge . 8)
          (background . "Minimize")
          (class . iconify-button))
         ((left-edge . 20)
          (top-edge . -9)
          (background . "Maximize")
          (class . maximize-button))
         ((left-edge . 32)
          (top-edge . -9)
          (class . shade-button)
          (background . "Shade2"))
         ((right-edge . -5)
          (top-edge . -1)
          (background . "br-shade")
          (class . bottom-right-corner)))))

     (mapping-alist
      '((default . "Default")
        (transient . "Default")
        (shaped . "Shaped")
        (shaped-transient . "Default")
        (unframed . "Unframed")))

     (theme-name 'Strap))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
