;; theme file, written Sat Mar 30 13:05:14 2002
;; created by sawfish-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("title-colors"
         (inactive . "#8c5f8e8c9554")
         (focused . "#ffffffffffff"))
        ("left-border"
         (inactive
          "left.png")
         (focused
          "left.png"))
        ("right-border"
         (inactive
          "right.png")
         (focused
          "right.png"))
        ("bottom-border"
         (inactive
          "bottom.png")
         (focused
          "bottom.png"))
        ("top-border"
         (inactive
          "top.png")
         (focused
          "top.png"))
        ("tl"
         (inactive
          "top-left.png")
         (focused
          "top-left.png"))
        ("tr"
         (inactive
          "top-right.png")
         (focused
          "top-right.png"))
        ("bl"
         (inactive
          "bottom-left.png")
         (focused
          "bottom-left.png"))
        ("br"
         (inactive
          "bottom-right.png")
         (focused
          "bottom-right.png"))
        ("title"
         (inactive . "#59995c286666")
         (focused
          "title.png"))
        ("min"
         (inactive
          "min.png")
         (focused
          "min.png")
         (highlighted
          "min-highlighted.png")
         (inactive-highlighted
          "min-highlighted.png")
         (clicked
          "min-clicked.png")
         (inactive-clicked
          "min-clicked.png"))
        ("max"
         (inactive
          "max.png")
         (focused
          "max.png")
         (highlighted
          "max-highlighted.png")
         (inactive-highlighted
          "max-highlighted.png")
         (clicked
          "max-clicked.png")
         (inactive-clicked
          "max-clicked.png"))
        ("menu"
         (inactive
          "menu.png")
         (focused
          "menu.png")
         (highlighted
          "menu-highlighted.png")
         (inactive-highlighted
          "menu-highlighted.png")
         (clicked
          "menu-clicked.png")
         (inactive-clicked
          "menu-clicked.png"))))

     (frames-alist
      '(("default-frame"
         ((top-edge . -18)
          (right-edge . 0)
          (left-edge . 0)
          (y-justify . 2)
          (x-justify . 24)
          (text . window-name)
          (foreground . "title-colors")
          (background . "title")
          (class . title))
         ((top-edge . -15)
          (left-edge . -4)
          (bottom-edge . 0)
          (background . "left-border")
          (class . left-border))
         ((right-edge . -4)
          (top-edge . -15)
          (bottom-edge . 0)
          (background . "right-border")
          (class . right-border))
         ((top-edge . -22)
          (left-edge . 0)
          (right-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((bottom-edge . -4)
          (left-edge . 0)
          (right-edge . 0)
          (background . "bottom-border")
          (class . bottom-border))
         ((top-edge . -22)
          (left-edge . -4)
          (background . "tl")
          (class . top-left-corner))
         ((right-edge . -4)
          (top-edge . -22)
          (background . "tr")
          (class . top-right-corner))
         ((bottom-edge . -4)
          (left-edge . -4)
          (background . "bl")
          (class . bottom-left-corner))
         ((right-edge . -4)
          (bottom-edge . -4)
          (background . "br")
          (class . bottom-right-corner))
         ((right-edge . 2)
          (top-edge . -17)
          (background . "menu")
          (class . close-button))
         ((right-edge . 20)
          (top-edge . -17)
          (background . "max")
          (class . maximize-button))
         ((top-edge . -17)
          (left-edge . 2)
          (background . "min")
          (class . iconify-button)))
        ("shaded"
         ((top-edge . -18)
          (left-edge . 0)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 24)
          (text . window-name)
          (foreground . "title-colors")
          (background . "title")
          (class . title))
         ((right-edge . 0)
          (left-edge . 0)
          (top-edge . 0)
          (background . "bottom-border")
          (class . bottom-border))
         ((top-edge . -1)
          (left-edge . -4)
          (background . "bl")
          (class . bottom-left-corner))
         ((right-edge . -4)
          (top-edge . -1)
          (background . "br")
          (class . bottom-right-corner))
         ((top-edge . -22)
          (left-edge . 0)
          (right-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((left-edge . -4)
          (top-edge . -22)
          (background . "tl")
          (class . top-left-corner))
         ((right-edge . -4)
          (top-edge . -22)
          (background . "tr")
          (class . top-right-corner))
         ((right-edge . 20)
          (top-edge . -17)
          (background . "max")
          (class . maximize-button))
         ((left-edge . 2)
          (top-edge . -17)
          (background . "min")
          (class . iconify-button))
         ((right-edge . 2)
          (top-edge . -17)
          (background . "menu")
          (class . close-button)))
        ("transient"
         ((left-edge . 0)
          (top-edge . -18)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (background . "title")
          (class . title))
         ((top-edge . -15)
          (left-edge . -4)
          (bottom-edge . 0)
          (background . "left-border")
          (class . left-border))
         ((right-edge . -4)
          (bottom-edge . 0)
          (top-edge . -15)
          (background . "right-border")
          (class . right-border))
         ((top-edge . -22)
          (right-edge . 0)
          (left-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((left-edge . 0)
          (right-edge . 0)
          (bottom-edge . -4)
          (background . "bottom-border")
          (class . bottom-border))
         ((left-edge . -4)
          (top-edge . -22)
          (background . "tl")
          (class . top-left-corner))
         ((right-edge . -4)
          (top-edge . -22)
          (background . "tr")
          (class . top-right-corner))
         ((left-edge . -4)
          (bottom-edge . -4)
          (background . "bl")
          (class . bottom-left-corner))
         ((bottom-edge . -4)
          (right-edge . -4)
          (background . "br")
          (class . bottom-right-corner)))
        ("shaded-transient"
         ((top-edge . -18)
          (right-edge . 0)
          (y-justify . 2)
          (x-justify . 8)
          (text . window-name)
          (foreground . "title-colors")
          (left-edge . 0)
          (background . "title")
          (class . title))
         ((left-edge . 0)
          (top-edge . -22)
          (right-edge . 0)
          (background . "top-border")
          (class . top-border))
         ((top-edge . -22)
          (left-edge . -4)
          (background . "tl")
          (class . top-left-corner))
         ((left-edge . -4)
          (top-edge . -1)
          (background . "bl")
          (class . bottom-left-corner))
         ((top-edge . -1)
          (right-edge . -4)
          (background . "br")
          (class . bottom-right-corner))
         ((top-edge . 0)
          (right-edge . 0)
          (left-edge . 0)
          (background . "bottom-border")
          (class . bottom-border))
         ((right-edge . -4)
          (top-edge . -22)
          (background . "tr")
          (class . top-right-corner)))))

     (mapping-alist
      '((default . "default-frame")
        (shaded . "shaded")
        (shaded-transient . "shaded-transient")
        (transient . "transient")))

     (theme-name 'gorillaz))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
