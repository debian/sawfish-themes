;; T-16-yellow for the 'Sawfish' Window Manager for X
;; by Gr1dl0ck (gr1dl0ck.deviantart.com)
;; email : abrehaut@bigfoot.com
;
; This file is part of the T-16-yellow Sawfish theme.
;
; T-16-yellow is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; T-16-yellow is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this theme; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


(let*
    ((title-width (lambda (w)
		    (let
			((w-width (car (window-dimensions w))))
		      (- (min (max (/ w-width 2) 100) w-width) 16))))
     
	;; Image Defintions (colorisations and scaling borders too) ====---     

     (top-images (mapcar (lambda (i)
			   (set-image-border i 32 8 1 1))
			 (list (make-image "top.png")
			       (make-image "top.png")
			       (make-image "top.png")
			       (make-image "top.png"))))

     (left-images (make-image "left.png"))
     (right-images (make-image "right.png"))

     (bottom-left-images (list (make-image "bottom-left.png")
			       (make-image "active-bottom-left.png")
                               (make-image "over-bottom-left.png")
                               (make-image "down-bottom-left.png")))
     (bottom-right-images (list (make-image "bottom-right.png")
				(make-image "active-bottom-right.png")
				(make-image "over-bottom-right.png")
				(make-image "down-bottom-right.png")))

     (bottom-images (set-image-border (make-image "bottom.png") 3 3 1 1))

     (title-images (mapcar (lambda (i)
                           (set-image-border i 3 3 1 1))
                         (list (make-image "title.png")
                               (make-image "active-title.png")
                               (make-image "active-title.png")
                               (make-image "active-title.png"))))

     (button-large-images (list (make-image "button-large.png")
                                (make-image "active-button-large.png")
                                (make-image "over-button-large.png")
				(make-image "down-button-large.png")))
     (button-medium-images (list (make-image "button-medium.png")
                                 (make-image "active-button-medium.png")
                                 (make-image "over-button-medium.png")
                                 (make-image "down-button-medium.png")))
     (button-small-images (make-image "button-small.png"))

     (black-image (make-image "black.png"))

	;; Frame Definitions =============---------

     (frame `(((background . ,top-images)
	       (left-edge . -8)
	       (right-edge . -5)
	       (top-edge . -30))
	      ((background . ,left-images)
	       (left-edge . -5)
	       (top-edge . 0)
	       (bottom-edge . 5)
	       (class . left-border))	
              ((background . ,right-images)
               (right-edge . -5)
               (top-edge . 0)
               (bottom-edge . 5)
	       (class . right-border)) 
              ((background . ,bottom-left-images)
               (left-edge . -5)
	       (bottom-edge . -6)
	       (class . bottom-left-corner)) 
              ((background . ,bottom-right-images)
               (right-edge . -5)
               (bottom-edge . -6)
               (class . bottom-right-corner))
	      ((background . ,bottom-images)
	       (left-edge . 17)
	       (right-edge . 17)
	       (bottom-edge . -6)
	       (class . bottom-border))

		;; Title =====------

              ((background . ,title-images)
	       (foreground . black)
               (text . ,window-name)
               (x-justify . 4)
               (y-justify . center)
               (left-edge . 25)
               (right-edge . 2)
               (top-edge . -19)
	       (class . title))

		;; buttons ====----
	      ((background . ,button-large-images)
	       (top-edge . -28)
	       (left-edge . 4)
	       (class . menu-button))
              ((background . ,button-medium-images)
               (top-edge . -13)
               (left-edge . -1)
               (class . iconify-button))
              ((background . ,button-small-images)
               (top-edge . -19)
               (left-edge . -6))
     ))

     (frame-shaped `(((background . ,top-images)
	       (left-edge . -8)
	       (right-edge . -5)
	       (top-edge . -30))

	      ((background . ,black-image)
	       (left-edge . -5)
	       (right-edge . -5)
	       (top-edge . -1)
	       (height . 2))

		;; Title =====------

              ((background . ,title-images)
	       (foreground . black)
               (text . ,window-name)
               (x-justify . 4)
               (y-justify . center)
               (left-edge . 25)
               (right-edge . 2)
               (top-edge . -19)
	       (class . title))

		;; buttons ====----
	      ((background . ,button-large-images)
	       (top-edge . -28)
	       (left-edge . 4)
	       (class . menu-button))
              ((background . ,button-medium-images)
               (top-edge . -13)
               (left-edge . -1)
               (class . iconify-button))
              ((background . ,button-small-images)
               (top-edge . -19)
               (left-edge . -6))
     ))

)

  (add-frame-style 'T-16-yellow
		   (lambda (w type)
		     (case type
		       ((default) frame)
		       ((transient) frame)
		       ((shaped) frame-shaped)
		       ((shaped-transient) frame-shaped)))))
