;; theme file, written Mon Jul 29 16:27:49 2002
;; created by sawfish-themer -- DO NOT EDIT!

(require 'make-theme)

(let
    ((patterns-alist
      '(("titlebar"
         (inactive
          "abutton1.png"
          (border
           6
           6
           6
           6))
         (focused
          "abutton3.png"
          (border
           6
           6
           6
           6))
         (highlighted
          "abutton2.png"
          (border
           6
           6
           6
           6))
         (inactive-highlighted
          "red.png"
          (border
           7
           7
           7
           7))
         (clicked
          "blue.png"
          (border
           6
           6
           6
           6))
         (inactive-clicked
          "abutton3.png"
          (border
           6
           6
           6
           6)))
        ("border"
         (inactive . "#be0ebe0ebe0e")
         (focused . "#917391739173")
         (highlighted . "#db25c99fa391")
         (inactive-highlighted . "#b2a8b83dad12")
         (clicked . "#9022e4d88b74"))
        ("closebutton"
         (inactive
          "close_overlay.png")
         (focused
          "close_overlay.png")
         (highlighted
          "close_overlay.png")
         (inactive-highlighted
          "close_overlay.png")
         (clicked
          "close_overlay.png")
         (inactive-clicked
          "close_overlay.png"))
        ("button"
         (inactive
          "abutton1.png"
          (border
           6
           6
           6
           6))
         (focused
          "abutton1.png"
          (border
           6
           6
           6
           6))
         (highlighted
          "abutton2.png"
          (border
           6
           6
           6
           6))
         (inactive-highlighted
          "abutton2.png"
          (border
           6
           6
           6
           6))
         (clicked
          "abutton3.png"
          (border
           6
           6
           6
           6))
         (inactive-clicked
          "abutton3.png"
          (border
           6
           6
           6
           6)))
        ("iconifybutton"
         (inactive
          "iconify_overlay.png")
         (focused
          "iconify_overlay.png")
         (highlighted
          "iconify_overlay.png")
         (inactive-highlighted
          "iconify_overlay.png")
         (clicked
          "iconify_overlay.png")
         (inactive-clicked
          "iconify_overlay.png"))
        ("maximizebutton"
         (inactive
          "maximize_overlay.png")
         (focused
          "maximize_overlay.png")
         (highlighted
          "maximize_overlay.png")
         (inactive-highlighted
          "maximize_overlay.png")
         (clicked
          "maximize_overlay.png")
         (inactive-clicked
          "maximize_overlay.png"))
        ("menubutton"
         (inactive
          "menu_overlay.png")
         (focused
          "menu_overlay.png")
         (highlighted
          "menu_overlay.png")
         (inactive-highlighted
          "menu_overlay.png")
         (clicked
          "menu_overlay.png")
         (inactive-clicked
          "menu_overlay.png"))
        ("shadebutton"
         (inactive
          "shade_overlay.png")
         (focused
          "shade_overlay.png")
         (highlighted
          "shade_overlay.png")
         (inactive-highlighted
          "shade_overlay.png")
         (clicked
          "shade_overlay.png")
         (inactive-clicked
          "shade_overlay.png"))
        ("stickybutton"
         (inactive
          "sticky_overlay.png")
         (focused
          "sticky_overlay.png")
         (highlighted
          "sticky_overlay.png")
         (inactive-highlighted
          "sticky_overlay.png")
         (clicked
          "sticky_overlay.png")
         (inactive-clicked
          "sticky_overlay.png"))
        ("bg"
         (inactive
          "bg.xpm")
         (focused
          "bg.xpm")
         (highlighted
          "bg.xpm")
         (inactive-highlighted
          "bg.xpm")
         (clicked
          "bg.xpm")
         (inactive-clicked
          "bg.xpm"))
        ("unstickybutton"
         (inactive
          "unsticky_overlay.png")
         (focused
          "unsticky_overlay.png")
         (highlighted
          "unsticky_overlay.png")
         (inactive-highlighted
          "unsticky_overlay.png")
         (clicked
          "unsticky_overlay.png")
         (inactive-clicked
          "unsticky_overlay.png"))))

     (frames-alist
      '(("default"
         ((font . "-adobe-helvetica-medium-r-normal-*-*-140-*-*-p-*-iso8859-15")
          (top-edge . -23)
          (height . 20)
          (right-edge . 60)
          (left-edge . 30)
          (y-justify . center)
          (x-justify . center)
          (background . "titlebar")
          (text . window-name)
          (class . title))
         ((right-edge . 0)
          (height . 20)
          (top-edge . -23)
          (background . "button")
          (foreground . "closebutton")
          (y-justify . center)
          (x-justify . center)
          (class . close-button))
         ((foreground . "iconifybutton")
          (background . "button")
          (height . 20)
          (top-edge . -23)
          (right-edge . 30)
          (y-justify . center)
          (x-justify . center)
          (text . "-")
          (class . iconify-button))
         ((height . 2)
          (right-edge . 0)
          (top-edge . -26)
          (left-edge . 0)
          (foreground . "border")
          (background . "border")
          (class . top-border))
         ((class . left-border)
          (top-edge . -24)
          (bottom-edge . 0)
          (left-edge . -2)
          (width . 2)
          (foreground . "border")
          (background . "border"))
         ((top-edge . -24)
          (right-edge . -2)
          (bottom-edge . 0)
          (width . 2)
          (foreground . "border")
          (background . "border")
          (class . right-border))
         ((left-edge . 0)
          (right-edge . 0)
          (bottom-edge . -2)
          (height . 2)
          (foreground . "border")
          (background . "border")
          (class . bottom-border))
         ((top-edge . -26)
          (height . 2)
          (width . 2)
          (left-edge . -2)
          (background . "border")
          (class . top-left-corner))
         ((top-edge . -26)
          (background . "border")
          (height . 2)
          (width . 2)
          (right-edge . -2)
          (class . top-right-corner))
         ((background . "border")
          (height . 2)
          (width . 2)
          (bottom-edge . -2)
          (left-edge . -2)
          (class . bottom-left-corner))
         ((background . "border")
          (height . 2)
          (width . 2)
          (bottom-edge . -2)
          (right-edge . -2)
          (class . bottom-right-corner))
         ((y-justify . center)
          (x-justify . center)
          (right-edge . 15)
          (top-edge . -23)
          (height . 20)
          (foreground . "maximizebutton")
          (background . "button")
          (class . maximize-button))
         ((y-justify . center)
          (x-justify . center)
          (right-edge . 45)
          (height . 20)
          (top-edge . -23)
          (class . shade-button)
          (foreground . "shadebutton")
          (background . "button"))
         ((height . 20)
          (y-justify . center)
          (x-justify . center)
          (top-edge . -23)
          (left-edge . 0)
          (foreground . "menubutton")
          (background . "button")
          (class . menu-button))
         ((height . 25)
          (top-edge . -25)
          (right-edge . -2)
          (left-edge . -2)
          (below-client . t)
          (background . "bg")
          (class . title))
         ((y-justify . center)
          (x-justify . center)
          (foreground . "stickybutton")
          (background . "button")
          (height . 20)
          (top-edge . -23)
          (left-edge . 15)
          (class . sticky-button)))
        ("shaped"
         ((right-edge . 30)
          (left-edge . 15)
          (font . "-adobe-helvetica-medium-r-normal-*-*-140-*-*-p-*-iso8859-15")
          (y-justify . center)
          (x-justify . center)
          (text . window-name)
          (background . "titlebar")
          (height . 20)
          (top-edge . -23)
          (class . title))
         ((left-edge . 0)
          (y-justify . center)
          (x-justify . center)
          (foreground . "menubutton")
          (background . "button")
          (height . 20)
          (top-edge . -23)
          (class . menu-button))
         ((right-edge . 15)
          (top-edge . -23)
          (height . 20)
          (y-justify . center)
          (x-justify . center)
          (foreground . "shadebutton")
          (background . "button")
          (class . shade-button))
         ((top-edge . -23)
          (height . 20)
          (y-justify . center)
          (x-justify . center)
          (foreground . "closebutton")
          (background . "button")
          (right-edge . 0)
          (class . close-button))
         ((height . 24)
          (top-edge . -25)
          (right-edge . -2)
          (left-edge . -2)
          (below-client . t)
          (background . "bg")
          (class . title)))))

     (mapping-alist
      '((default . "default")
        (transient . "default")
        (shaped-transient . "shaped")
        (shaped . "shaped")))

     (theme-name 'Bubbles_Exact))

  (add-frame-style
   theme-name (make-theme patterns-alist frames-alist mapping-alist))
  (when (boundp 'mark-frame-style-editable)
    (mark-frame-style-editable theme-name)))
