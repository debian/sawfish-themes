; Aquiline/theme.jl
;; Original Aquaesque theme:
;; Copyright (C) 2000 Henrik Magnusson <henma887@student.liu.se>
;; Aquiline theme:
;; Modifications (C) 2001 Matt Chisholm <matt@theory.org>

(let*
    ;; Update window title pixel length
    ((title-width
      (lambda (w)
	(let
	    ((w-width (car (window-dimensions w))))
	  (max 0 (min (- w-width 100) (text-width (window-name w)))))))
     
     (font (get-font "-*-lucida-medium-r-normal-*-*-100-*-*-p-*-iso8859-1"))

     (font-colors (list "white" "black"))

     (top-left-images (list (make-image "top_left_inactive.png")
	   		    (make-image "top_left.png")))

     (top-left-shaded-images (list (make-image "top_left_inactive_shaded.png")
	                           (make-image "top_left_shaded.png")))
     
     (top-title-images (list (make-image "top_title_inactive.png")
	                     (make-image "top_title.png")))

     (top-gradient-images (list (make-image "top_gradient_inactive.png")
	       			(make-image "top_gradient.png")))

     (top-right-images (list (make-image "top_right_inactive.png")
	       	             (make-image "top_right.png")))

     (top-right-shaded-images (list (make-image "top_right_inactive_shaded.png")
	       		            (make-image "top_right_shaded.png")))

     (close-images (list (make-image "close_normal.png")
	                 (make-image "close_active.png")
               		 (make-image "close_high.png")
               		 (make-image "close_pressed.png")))

     (maximize-images (list (make-image "max_normal.png")
	            	    (make-image "max_active.png")
                     	    (make-image "max_high.png")
	                    (make-image "max_pressed.png")))

     (iconify-images (list (make-image "min_normal.png")
		           (make-image "min_active.png")
        	           (make-image "min_high.png")
	                   (make-image "min_pressed.png")))

     (border-images (make-image "bl.png"))

     ;; Frame definitions:
     (frame `(
	      ;; top left corner
	      ((background . ,top-left-images)
	       (left-edge . -2)
	       (top-edge . -23)
	       (class . top-left-corner))

	      ;; behind title
	      ((background . ,top-gradient-images)
	       ;;             top-title-images
	       (foreground . ,font-colors)
	       (text . ,window-name)
	       (x-justify . 4)
	       (y-justify . 6)
	       (top-edge . -23)
	       (left-edge . 5) 
	       (right-edge . 35) ;; comment out
	       (class . title))

	      ;; iconify button
	      ((background . ,iconify-images)
	       (right-edge . 37)
	       (top-edge . -23)
	       (class . iconify-button))

	      ;; maximize button
	      ((background . ,maximize-images)
	       (right-edge . 20)
	       (top-edge . -23)
	       (class . maximize-button))

	      ;; delete button
	      ((background . ,close-images)
	       (right-edge . 2)
	       (top-edge . -23)
	       (class . close-button))

	      ;; top-right corner
	      ((background . ,top-right-images)
	       (right-edge . -2)
	       (top-edge . -23)
	       (class . top-right-corner))

	      ;; left border
	      ((background . ,border-images)
	       (left-edge . -2)
	       (top-edge . 0)
	       (bottom-edge . 0)
	       (class . left-border))

	      ;; right border
	      ((background . ,border-images)
	       (right-edge . -2)
	       (top-edge . 0)
	       (bottom-edge . 0)
	       (class . right-border))

	      ;; bottom border
	      ((background . ,border-images)
	       (left-edge . 0)
	       (right-edge . 0)
	       (bottom-edge . -2)
	       (class . bottom-border))

	      ;; bottom-left corner
	      ((background . ,border-images)
	       (left-edge . -1)
	       (bottom-edge . -1)
	       (class . bottom-left-corner))

	      ;; bottom-right corner
	      ((background . ,border-images)
	       (right-edge . -1)
	       (bottom-edge . -1)
	       (class . bottom-right-corner))))

     (shaded-frame `(
		     ;; top left corner
		     ((background . ,top-left-shaded-images)
		      (left-edge . -2)
		      (top-edge . -23)
		      (class . top-left-corner))

		     ;; behind title
		     ((background . ,top-title-images)
		      (foreground . ,font-colors)
		      (text . ,window-name)
		      (x-justify . 4)
		      (y-justify . 6)
		      (top-edge . -23)
		      (left-edge . 5)
		      (width . ,(lambda (w) (+ (title-width w) 13)))
		      (class . title))

		     ;; gradient
		     ((background . ,top-gradient-images)
		      (top-edge . -23)
		      (left-edge . ,(lambda (w) (+ (title-width w) 13)))
		      (right-edge . 35)
		      (class . title))

		     ;; iconify button
		     ((background . ,iconify-images)
	      	      (right-edge . 37)
		      (top-edge . -23)
		      (class . iconify-button))

		     ;; maximize button
		     ((background . ,maximize-images)
		      (right-edge . 20)
		      (top-edge . -23)
		      (class . maximize-button))

		     ;; delete button
		     ((background . ,close-images)
		      (right-edge . 2)
		      (top-edge . -23)
		      (class . close-button))

		     ;; top-right corner
		     ((background . ,top-right-shaded-images)
		      (right-edge . -2)
		      (top-edge . -23)
		      (class . top-right-corner))))
     
     (transient-frame `(
			;; top left corner
			((background . ,top-left-images)
			 (left-edge . -2)
			 (top-edge . -23)
			 (class . top-left-corner))

			;; behind title
			((background . ,top-gradient-images)
			 ;;             top-title-images
			 (foreground . ,font-colors)
			 (text . ,window-name)
			 (x-justify . 4)
			 (y-justify . 6)
			 (top-edge . -23)
			 (left-edge . 5)
			 (right-edge . 35) ;; comment out
			 (class . title))

			;; iconify button
	 	        ((background . ,iconify-images)
		         (right-edge . 37)
	                 (top-edge . -23)
	                 (class . iconify-button))

			;; maximize button
			((background . ,maximize-images)
			 (right-edge . 20)
			 (top-edge . -23)
			 (class . maximize-button))

			;; delete button
			((background . ,close-images)
			 (right-edge . 2)
			 (top-edge . -23)
			 (class . close-button))

			;; top-right corner
			((background . ,top-right-images)
			 (right-edge . -2)
			 (top-edge . -23)
			 (class . top-right-corner))

			;; left border
			((background . ,border-images)
			 (left-edge . -2)
			 (top-edge . 0)
			 (bottom-edge . 0)
			 (class . left-border))

			;; right border
			((background . ,border-images)
			 (right-edge . -2)
			 (top-edge . 0)
			 (bottom-edge . 0)
			 (class . right-border))

			;; bottom border
			((background . ,border-images)
			 (left-edge . 0)
			 (right-edge . 0)
			 (bottom-edge . -2)
			 (class . bottom-border))

			;; bottom-left corner
			((background . ,border-images)
			 (left-edge . -1)
			 (bottom-edge . -1)
			 (class . bottom-left-corner))

			;; bottom-right corner
			((background . ,border-images)
			 (right-edge . -1)
			 (bottom-edge . -1)
			 (class . bottom-right-corner))))

     (shaded-transient-frame `(
			       ;; top left corner
			       ((background . ,top-left-shaded-images)
				(left-edge . -2)
				(top-edge . -23)
				(class . top-left-corner))

			       ;; behind title
			       ((background . ,top-title-images)
				(foreground . ,font-colors)
				(text . ,window-name)
				(x-justify . 4)
				(y-justify . 6)
				(top-edge . -23)
				(left-edge . 5)
				(width . ,(lambda (w) (+ (title-width w) 13)))
				(class . title))

			       ;; gradient
			       ((background . ,top-gradient-images)
				(top-edge . -23)
				(left-edge . ,(lambda (w) (+ (title-width w) 13)))
				(right-edge . 35)
				(class . title))

			       ;; maximize button
	 	               ((background . ,maximize-images)
		                (right-edge . 37)
	                        (top-edge . -23)
	                        (class . maximize-button))

			       ;; maximize button
			       ((background . ,maximize-images)
				(right-edge . 20)
				(top-edge . -23)
				(class . maximize-button))

			       ;; delete button
			       ((background . ,close-images)
				(right-edge . 2)
				(top-edge . -23)
				(class . close-button))

			       ;; top-right corner
			       ((background . ,top-right-shaded-images)
				(right-edge . -2)
				(top-edge . -23)
				(class . top-right-corner)))))
  
  (add-frame-style 'Aquiline
		   (lambda (w type)
		     (cond ((eq type 'shaded)
			    shaded-frame)
			   ((eq type 'transient)
			    transient-frame)
			   ((eq type 'shaded-transient)
			    shaded-transient-frame)
			   ((eq type 'unframed)
			    nil-frame)
			   (t
			    frame))))
  
  (call-after-property-changed
   'WM_NAME (lambda ()
	      (rebuild-frames-with-style 'Aquiline))))