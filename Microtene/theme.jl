;; Microtene/theme.jl

;; Copyright (C) 2000 Martin Kavalec <martin@penguin.cz>

;; you can use, modify and distribute this file under the terms
;; od GNU General Public License

;; icon images are taken form the Helix theme (then modified)
;; Helix theme is by Tuomas Kuosmanen <tigert@gimp.org>

;; this file is written analogically to
;; brushed-metal/theme.jl from the sawfish distribution

(let*
  (
    (font (get-font "-*-lucida-medium-r-normal-*-*-100-*-*-p-*-iso8859-2"))
    (font-colors (list "black" "white"))
    
	(title-images (list (make-image "title-inactive.png")
                            (make-image "title.png")))

	(title-left (list (make-image "title-left-i.png")
			    (make-image "title-left.png")))
	
	(title-right (list (make-image "title-right-i.png")
			    (make-image "title-right.png")))


    	(iconify-images (list (make-image "button-left.png")
			      nil nil
                              (make-image "button-left-c.png")))

     	(close-images (list (make-image "button-right.png")
			    nil nil
                            (make-image "button-right-c.png")))

     	(border-bottom (make-image "bottom.png"))
     	(border-right (make-image "right.png"))
     	(border-left (make-image "left.png"))

     	(top-left (make-image "top-left.png"))
    	(top-right (make-image "top-right.png"))
     	(top-lefts (make-image "top-left-shaded.png"))
    	(top-right-shaded (make-image "top-right-shaded.png"))
     	(corner-bl (make-image "bottom-left.png"))
     	(corner-br (make-image "bottom-right.png"))

     	(frame `(
		((background . ,top-left)
		 (top-edge . -14)
		 (left-edge . -4)
		 (class . top-left-corner))
		
		((background . ,iconify-images)
		 (top-edge . -14)
		 (left-edge . 0)
		 (class . iconify-button))

		((background . ,title-left)
		 (top-edge . -14)
		 (left-edge . 16)
		 (class . title))

		((background . ,title-images)
		 (foreground . ,font-colors)
		 (font . ,font)
		 (text . ,window-name)
		 (x-justify . 4)
		 (y-justify . center)
		 (top-edge . -14)
		 (left-edge . 17)
		 (right-edge . 17)
		 (class . title))
		
		((background . ,title-right)
		 (top-edge . -14)
		 (right-edge . 16)
		 (class . title))

    		((background . ,close-images)
      		 (top-edge . -14)
      		 (right-edge . 0)
      		 (class . close-button))
		
		((background . ,top-right)
		 (top-edge . -14)
		 (right-edge . -4)
		 (class . top-right-corner))

     		((background . ,border-left)
      		 (top-edge . 0)
      		 (bottom-edge . 0)
      		 (left-edge . -4) 
      		 (class . left-border))

     		((background . ,border-right)
      		 (top-edge . 0)
      		 (bottom-edge . 0)
      		 (right-edge . -4)
      		 (class . right-border))

     		((background . ,border-bottom)
      		 (bottom-edge . -4)
      		 (right-edge . 16)  
      		 (left-edge . 16)   
      		 (class . bottom-border))

     		((background . ,corner-bl)
      		 (bottom-edge . -4)
      		 (left-edge . -4)  
      		 (class . bottom-left-corner))

     		((background . ,corner-br)
      		 (bottom-edge . -4)
      		 (right-edge . -4) 
      		 (class . bottom-right-corner))
	))

	(shaped-frame `(
		((background . ,top-lefts)
		 (top-edge . -14)
		 (left-edge . -4)
		 (class . top-left-corner))

		((background . ,iconify-images)
		 (top-edge . -14)
		 (left-edge . 0)
		 (class . iconify-button))

		((background . ,title-left)
		 (top-edge . -14)
		 (left-edge . 16)
		 (class . title))

		((background . ,title-images)
		 (foreground . ,font-colors)
		 (font . ,font)
		 (text . ,window-name)
		 (x-justify . 4)
		 (y-justify . center)
		 (top-edge . -14)
		 (left-edge . 17)
		 (right-edge . 17)
		 (class . title))
		
		((background . ,title-right)
		 (top-edge . -14)
		 (right-edge . 16)
		 (class . title))

    		((background . ,close-images)
      		 (top-edge . -14)
      		 (right-edge . 0)
      		 (class . close-button))

		((background . ,top-right-shaded)
		 (top-edge . -14)
		 (right-edge . -4)
		 (class . top-right-corner))
	))

	(transient-frame `(
		((background . ,title-right)
		 (top-edge . -14)
		 (left-edge . -4)
		 (class . title))
		((background . ,title-left)
		 (top-edge . -14)
		 (left-edge . -3)
		 (class . title))

		((background . ,title-images)
		 (foreground . ,font-colors)
		 (font . ,font)
		 (text . ,window-name)
		 (x-justify . 4)
		 (y-justify . center)
		 (top-edge . -14)
		 (left-edge . -2)
		 (right-edge . 17)
		 (class . title))
		
		((background . ,title-right)
		 (top-edge . -14)
		 (right-edge . 16)
		 (class . title))

    		((background . ,close-images)
      		 (top-edge . -14)
      		 (right-edge . 0)
      		 (class . close-button))
		
		((background . ,top-right)
		 (top-edge . -14)
		 (right-edge . -4)
		 (class . top-right-corner))

     		((background . ,border-left)
      		 (top-edge . 0)
      		 (bottom-edge . 0)
      		 (left-edge . -4) 
      		 (class . left-border))

     		((background . ,border-right)
      		 (top-edge . 0)
      		 (bottom-edge . 0)
      		 (right-edge . -4)
      		 (class . right-border))

     		((background . ,border-bottom)
      		 (bottom-edge . -4)
      		 (right-edge . 16)  
      		 (left-edge . 16)   
      		 (class . bottom-border))

     		((background . ,corner-bl)
      		 (bottom-edge . -4)
      		 (left-edge . -4)  
      		 (class . bottom-left-corner))

     		((background . ,corner-br)
      		 (bottom-edge . -4)
      		 (right-edge . -4) 
      		 (class . bottom-right-corner))
	))
	
	(shaped-transient-frame `(
		((background . ,title-right)
		 (top-edge . -14)
		 (left-edge . -4)
		 (class . title))

		((background . ,title-left)
		 (top-edge . -14)
		 (left-edge . -3)
		 (class . title))

     		((background . ,title-images)
      		 (foreground . ,font-colors)
      		 (font . ,font)
      		 (text . ,window-name)
      		 (x-justify . 4)
      		 (y-justify . center)
      		 (top-edge . -14)
      		 (left-edge . -2)
      		 (right-edge . 17)
      		 (class . title))
		
		((background . ,title-right)
		 (top-edge . -14)
		 (right-edge . 16)
		 (class . title))

     		((background . ,close-images)
      		 (top-edge . -14)
      		 (right-edge . 0)
      		 (class . close-button))
		
		((background . ,top-right-shaded)
		 (top-edge . -14)
		 (right-edge . -4)
		 (class . top-right-corner))

	))
	
  )
  (add-frame-style 'Microtene
    	(lambda (w type)   
          (case type
	      ((default) frame)
	      ((shaped) shaped-frame)
              ((transient) transient-frame)
              ((shaped-transient) shaped-transient-frame)
              ((unframed) nil-frame)))))
